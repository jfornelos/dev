<?php

class RestAPI{

    /***********************Constants********************************/

    static $BASE_URL_SANDBOX = "https://standvirtual.fixeads.com/api/open";
    static $BASE_URL_PRODUCTION = "https://www.standvirtual.com/api/open";

    static $CONTENT_TYPE_JSON ="application/json";
    static $CONTENT_TYPE_URL ="Content-Type: application/x-www-form-urlencoded";
    static $BEARER = "Authorization: Bearer ";

    static $CONCAT_CHARACTER = "&";
    static $DIRECTORY_CHARACTER = "/";

    //CLIENT AUTHENTICATION

    static $POST_AUTH_URL = "/oauth/token";
    static $CLIENT_ID = "93";
    static $CLIENT_SECRET = "181b7172f28024d88d3eb0810edaab86";
    static $PARTNER_CODE = "partner_code";
    static $PARTNER_SECRET = "partner_secret";
    static $GRANT_TYPE = "grant_type";
    static $GRANT_TYPE_PARTNER = "partner";
    static $GRANT_TYPE_PASSWORD = "password";
    static $USERNAME = "username";
    static $PASSWORD = "password";

    //LOCATION
    //Region
    static $GET_REGIONS = "/regions";
    static $GET_REGION_ID_DATA = "/regions/";
    //City
    static $GET_CITIES = "/cities";
    static $GET_CITY_ID_DATA = "/cities/";
    static $POST_CITIES = "/cities/search";
    //Districts
    static $GET_DISTRICTS = "/districts";
    static $GET_ALLCITIES_DISTRICT = "/districts/for-city-id/";
    static $GET_DISTRICT_ID_DATA = "/districts/";
    //Country
    static $GET_COUNTRIES = "/countries";
    static $GET_COUNTRY_ID_DATA = "/countries/";


    //IMAGES
    static $POST_IMAGE_COLLECTION = "/imageCollections";
    static $IMAGE_COLLECTION = "/imageCollections/";
    static $IMAGE_TO_COLLECTION ="/images/";

    //CATEGORIES
    static $CATEGORIES = "/categories/";
    static $MODELS = "/models/";
    static $ENGINES = "/engines/";
    static $MAKES = "/makes";
    static $VERSIONS = "/versions/";
    static $CATEGORIES_PARAMETER = "?all=";
    static $CATEGORIES_PAGE_PARAMETER = "?page=";
    static $CATEGORIES_LIMIT_PARAMETER = "?limit=";

    //STANDS
    static $POST_STAND ="/stands";
    static $STANDS ="/stands/";
    static $GET_STANDS = "/stands/getStandsByUserId/";

    //ADVERT
    static $POST_ADVERT ="/adverts";
    static $ADVERT = "/adverts/";
    static $ADVERT_EXTERNAL = "/externalId";

    //ACCOUNT
    static $NEW_ADVERT = "/account/adverts";
    static $NEW_ADVERT_LINK = "/account/adverts/";
    static $ADVERT_ACTIVATE = "/activate";
    static $ADVERT_DEACTIVATE = "/deactivate";
    static $ADVERT_DETAILS = "/stats/details";
    static $ADVERT_STATS = "/stats";
    static $ADVERT_CURRENT_STATS = "/account/stats/details";
    static $ADVERT_ACCOUNT_STATS = "/account/stats";
    static $POST_ADVERT_STATUS = "/account/status";

    //PROMOTIONS
    static $PROMOTIONS = "/promotions/";


    /***********************Variables********************************/
    private $isPartner;
    private $tokenData;

    /****************Constructor Gets and Sets**********************/
    /**
     * @param partner $boolean true if user is partner false if is normal user
     */
    function __construct($partner) 
    { 
        $this->isPartner = $partner;
    } 

    public function __destruct() {
    }

    /**
     * @return true $if user is partner
     * @return false $if is normal user
     */
    public function IsPartner(){
        return $this->isPartner;
    }

    /**
     * @return tokenData $all user data received from user authentication, includes token type, value and expiring date
     */
    public function getTokenData(){
        return $this->tokenData;
    }
    /**
     * Session token is saved
     * 
     * @return currentToken
     */
    public function getTokenKey(){
        return $this->tokenData->access_token;
    }

    /**
     * @param data $data received from user authentication
     */
    public function setTokenData($data){
        $this->tokenData = json_decode($data['data']);
    }

    /***********************Rest Methods********************************/

    public function post($url, $data, $contentType){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $contentType);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $output = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        
       

        $result = array('httpCode'=> $httpCode,'data'=>$output);

        echo $result['data'];

        return $result;
    }

    public function get($url, $contentType){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $contentType);
        $output = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $result = array('httpCode'=> $httpCode,'data'=>$output);

        return $result;
    }

    public function put($url, $data, $contentType){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $contentType);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $output = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $result = array('httpCode'=> $httpCode,'data'=>$output);

        return $result;
    }

    public function delete($url, $contentType)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $contentType);
        $output = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $result = array('httpCode'=> $httpCode,'data'=>$output);
    
        return $result;
    }



    /***********************User Authentication*********************************/

    private function baseClientSecret()
    {
        $data = array('client_id'=>  $this::$CLIENT_ID,'client_secret'=>$this::$CLIENT_SECRET);                               
        return  $data;
    }

    /**
     * User authentication- Token type 'Bearer' only
     * @param username
     * @param password
     * 
     * @return access token data
     */
    public function userAuthentication($username, $password)
    {
        $baseBody = $this->baseClientSecret();

        $url =  $this::$BASE_URL_SANDBOX . $this::$POST_AUTH_URL;

        $baseBody[$this::$USERNAME] = $username;
        $baseBody[$this::$PASSWORD] = $password;
        $baseBody[$this::$GRANT_TYPE] = $this::$GRANT_TYPE_PASSWORD;

        return $this->post($url,$baseBody, array($this::$CONTENT_TYPE_URL));
    }

    /**
     * Partner authentication - Token type 'Bearer' only
     * @param partner_code
     * @param partner_secret
     * 
     * @return access token data
     */
    public function partnerAuthentication($partner_code, $partner_secret)
    {
        $baseBody = $this->baseClientSecret();


        $url =  $this::$BASE_URL_SANDBOX . $this::$POST_AUTH_URL;

        $baseBody[$this::$PARTNER_CODE] = $partner_code;
        $baseBody[$this::$PARTNER_SECRET] = $partner_secret;
        $baseBody[$this::$GRANT_TYPE] = $this::$GRANT_TYPE_PARTNER;

        return $this->post($url,$baseBody, array($this::$CONTENT_TYPE_URL));

    }

    /***********************Location*********************************/
    /**
     * @return array $array with httpCode and data of all regions available
     */
    public function getAllRegions()
    {
        $url =  $this::$BASE_URL_SANDBOX . $this::$GET_REGIONS;
        
        $token = "Authorization: Bearer " . $this->getTokenKey();

        return $this->get($url,array($this::$CONTENT_TYPE_JSON, $token));
    }

    /**
    * @param regionID $ region unique identifier
    * 
    * @return array $array with httpCode and data of selected region
    */
    public function getRegionData($regionID){

        $url =  $this::$BASE_URL_SANDBOX . $this::$GET_REGION_ID_DATA . $id;
        
        $token = "Authorization: Bearer " . $this->getTokenKey();

        return $this->get($url,array($this::$CONTENT_TYPE_JSON, $token));
    }

    /**
     * @return array $array with httpCode and data of all available cities
    */
    public function getAllCities()
    {
        $url =  $this::$BASE_URL_SANDBOX . $this::$GET_CITIES;
        
        $token = "Authorization: Bearer " . $this->getTokenKey();

        return $this->get($url,array($this::$CONTENT_TYPE_JSON, $token));
    }

    /**
     * @return array $array with httpCode and data with details of all cities
     */
    public function postAllCities()
    {
        $url =  $this::$BASE_URL_SANDBOX . $this::$POST_CITIES;
        
        $token = "Authorization: Bearer " . $this->getTokenKey();
        $data = array();
        return $this->post($url, $data, array($this::$CONTENT_TYPE_JSON, $token));
    }

    /**
    * @param cityID $ city unique identifier
    * 
    * @return array $array with httpCode and data details of city 
    */
    public function getCityData($cityID){

        $url =  $this::$BASE_URL_SANDBOX . $this::$GET_CITY_ID_DATA . $id;
        
        $token = "Authorization: Bearer " . $this->getTokenKey();

        return $this->get($url,array($this::$CONTENT_TYPE_JSON, $token));
    }

    
    /**
     * @return array $array with httpCode and data with details of all districts
     */
    public function getAllDistricts()
    {
        $url =  $this::$BASE_URL_SANDBOX . $this::$GET_DISTRICTS;
        echo $url;
        $token = "Authorization: Bearer " . $this->getTokenKey();

        return $this->get($url,array($this::$CONTENT_TYPE_JSON, $token));
    }

    /**
    * @param cityID $ city unique identifier
    * 
    * @return array $array with httpCode and data details of all city districts 
    */
    public function getAllCityDistricts($cityID){

        $url =  $this::$BASE_URL_SANDBOX . $this::$GET_ALLCITIES_DISTRICT . $id;
        echo $url;
        $token = "Authorization: Bearer " . $this->getTokenKey();

        return $this->get($url,array($this::$CONTENT_TYPE_JSON, $token));
    }

     /**
    * @param districtID $ district unique identifier
    * 
    * @return array $array with httpCode and data details of district
    */
    public function getDistrictData($districtID){

        $url =  $this::$BASE_URL_SANDBOX . $this::$GET_DISTRICTS . $id;
        
        $token = "Authorization: Bearer " . $this->getTokenKey();

        return $this->get($url,array($this::$CONTENT_TYPE_JSON, $token));
    }

     /**
     * @return array $array with httpCode and data with all countries
     */
    public function getAllCountries()
    {
        $url =  $this::$BASE_URL_SANDBOX . $this::$GET_COUNTRIES;
        echo $url;
        $token = "Authorization: Bearer " . $this->getTokenKey();

        return $this->get($url,array($this::$CONTENT_TYPE_JSON, $token));
    }

      /**
    * @param countryID $ country unique identifier
    * 
    * @return array $array with httpCode and data details of selected country
    */
    public function getCountryData($countryID){

        $url =  $this::$BASE_URL_SANDBOX . $this::$GET_COUNTRY_ID_DATA . $id;
        echo $url;
        $token = "Authorization: Bearer " . $this->getTokenKey();

        return $this->get($url,array($this::$CONTENT_TYPE_JSON, $token));
    }


      /***********************Categories*********************************/

    /**
    * @param all $ all=1 returns all categories (also children ones) in sigle request
    * 
    * @return array $array with httpCode and data with all categories
    */
      public function getAllCategories($all)
      {
        if($all == 1)
            $url =  $this::$BASE_URL_SANDBOX . $this::$CATEGORIES . $this::$CATEGORIES_PARAMETER . $all;
        else
             $url =  $this::$BASE_URL_SANDBOX . $this::$CATEGORIES;
        
        $token = "Authorization: Bearer " . $this->getTokenKey();
  
        return $this->get($url,array($this::$CONTENT_TYPE_JSON, $token));
      }

    /**
    * @param id $ category unique id
    * 
    * @return array $array with httpCode and data details of category of given id
    */
      public function getCategoryData($id){

        $url =  $this::$BASE_URL_SANDBOX . $this::$CATEGORIES . $id;
        echo $url;
        $token = "Authorization: Bearer " . $this->getTokenKey();

        return $this->get($url,array($this::$CONTENT_TYPE_JSON, $token));
    }

    /**
     * @param categoryID $ unique category ID
     * @param brandCode $ Brand Slug ex:mercedes-benz
     * @param modelCode $ Version Slug ex:c-klasa
     * 
     * @return array $array with httpCode and data with list of all engine codes from model
     */
    public function getAllEngineCodeFromModel($categoryID, $brandCode, $modelCode)
    {
        $url =  $this::$BASE_URL_SANDBOX . $this::$CATEGORIES . $categoryID . $this::$MODELS . $brandCode . $this::$ENGINES . $modelCode;
        echo $url;
        $token = "Authorization: Bearer " . $this->getTokenKey();

        return $this->get($url,array($this::$CONTENT_TYPE_JSON, $token));
    }

    /**
     * @param categoryID $ unique category ID
     * @param brandCode $ Brand Slug ex:mercedes-benz
     * 
     * @return array $array with httpCode and data with list of all models from vehicle brand
     */
    public function getAllModelsFromBrand($categoryID, $brandCode)
    {
        $url =  $this::$BASE_URL_SANDBOX . $this::$CATEGORIES . $categoryID . $this::$MODELS . $brandCode;
        echo $url;
        $token = "Authorization: Bearer " . $this->getTokenKey();

        return $this->get($url,array($this::$CONTENT_TYPE_JSON, $token));
    }

    
    /**
     * @param categoryID $ unique category ID
     * 
     * @return array $array with httpCode and data with list of all vehicles brands in category
     */
    public function getAllVehiclesBrandsInCategory($categoryID)
    {
        $url =  $this::$BASE_URL_SANDBOX . $this::$CATEGORIES . $categoryID . $this::$MAKES;
        echo $url;
        $token = "Authorization: Bearer " . $this->getTokenKey();

        return $this->get($url,array($this::$CONTENT_TYPE_JSON, $token));
    }

    /**
     * @param categoryID $ unique category ID
     * @param brandCode $ Brand Slug ex:mercedes-benz
     * @param modelCode $ Version Slug ex:c-klasa
     * 
     * @return array $array with httpCode and data with list of all versions from model
     */
    public function getAllVersionsFromModel($categoryID, $brandCode, $modelCode)
    {
        $url =  $this::$BASE_URL_SANDBOX . $this::$CATEGORIES . $categoryID . $this::$MODELS . $brandCode . $this::$VERSIONS . $modelCode;
        echo $url;
        $token = "Authorization: Bearer " . $this->getTokenKey();

        return $this->get($url,array($this::$CONTENT_TYPE_JSON, $token));
    }

     /***********************Images*********************************/

    /**
     * @param imagesData $ array with the following format to create a new image collection
     *      {
     *          "1": "http://lorempixel.com/800/600/transport/",
     *          "2": "http://lorempixel.com/800/600/transport/",
     *          "3": "http://lorempixel.com/800/600/transport/"
     *       }
     *       
     * @return array $array with httpCode and data with image collection link
     */
     public function postImageCollection($imagesData)
     {
        $url =  $this::$BASE_URL_SANDBOX . $this::$POST_IMAGE_COLLECTION;
        
        $token = "Authorization: Bearer " . $this->getTokenKey();
       
        return $this->post($url, $imagesData, array($this::$CONTENT_TYPE_JSON, $token));
     }

     /**
      * @param collectionID $ unique image collection identifier
      *
      * @return array $array with httpCode and data details of selected image collection
      */
     public function getImageCollection($collectionID)
     {
        $url =  $this::$BASE_URL_SANDBOX . $this::$IMAGE_COLLECTION . $collectionID;
        
        $token = "Authorization: Bearer " . $this->getTokenKey();

        return $this->get($url,array($this::$CONTENT_TYPE_JSON, $token));
     }

    /**
      * @param collectionID $ unique image collection identifier
      * @param imageID $ unique image collection identifier
      *
      * @return array $array with httpCode and data with image details
      */
     public function getImageFromCollection($collectionID, $imageID)
     {
        $url =  $this::$BASE_URL_SANDBOX . $this::$IMAGE_COLLECTION . $collectionID . $this::$IMAGE_TO_COLLECTION . $imageID;
        
        $token = "Authorization: Bearer " . $this->getTokenKey();

        return $this->get($url,array($this::$CONTENT_TYPE_JSON, $token));
     }

     
     /**
      * @param collectionID $ unique image collection identifier
      * @param imageData $ array with the following format
      *   {
      *      "source": "http://lorempixel.com/800/600/abstract/"
      *  }
      *
      * @return array $array with httpCode and data details of new image
      */
     public function putNewImage($collectionID, $imageData)
     {
        $url =  $this::$BASE_URL_SANDBOX . $this::$IMAGE_COLLECTION . $collectionID . $this::$IMAGE_TO_COLLECTION;
      
        $token = "Authorization: Bearer " . $this->getTokenKey();
       
        return $this->put($url, $imageData, array($this::$CONTENT_TYPE_JSON, $token));
     }

     /**
      * @param collectionID $ unique image collection identifier
      * @param imageID $ unique image identifier
      * @param imageData $ array with the following format
      *   {
      *      "source": "http://lorempixel.com/800/600/abstract/"
      *  }
      *
      * @return array $array with httpCode and data details of new image
      */
     public function putUpdateImage($collectionID, $imageID, $imageData)
     {
        $url =  $this::$BASE_URL_SANDBOX . $this::$IMAGE_COLLECTION . $collectionID . $this::$IMAGE_TO_COLLECTION . $imageID;
   
        $token = "Authorization: Bearer " . $this->getTokenKey();

        return $this->put($url, $imageData, array($this::$CONTENT_TYPE_JSON, $token));
     }

    /**
      * @param collectionID $ unique image collection identifier
      * @param imageID $ unique image identifier
      *
      * @return array $array with httpCode 204 if successful and empty data
      */
     public function deleteImageFromCollection($collectionID, $imageID)
     {
        $url =  $this::$BASE_URL_SANDBOX . $this::$IMAGE_COLLECTION . $collectionID . $this::$IMAGE_TO_COLLECTION . $imageID;

        $token = "Authorization: Bearer " . $this->getTokenKey();

        return $this->delete($url, array($this::$CONTENT_TYPE_JSON, $token));
     }

      /***********************Stands*********************************/

      /**
       * @require ONLY FOR PARTNER
      * @param standData $ array with the following format
      * {
      *      'name'     => 'Name of the stand',
      *      'status'   => 'active',
      *      'address'  => 'Stand address',
      *      'postcode' => '39384-213',
      *      'email'    => 'standemail@gmail.com',
      *      'phone1'   => '00441892892',
      *      'phone2'   => '00441892893',
      *      'phone3'   => '00441892894',
      *      'userId'   => '1234',
      *      'cityId'   => '123123',
      *      'mapLat'   => '40.200537',
      *      'mapLon'   => '-8.242188',
      *  }
      *
      * @return array  $array with httpCode and data details of new stand
      */
      public function postNewStand($standData)
      {
        $url =  $this::$BASE_URL_SANDBOX . $this::$POST_STAND;
        
        $token = "Authorization: Bearer " . $this->getTokenKey();
       
        return $this->post($url, $standData, array($this::$CONTENT_TYPE_JSON, $token));
      }

      /**
      * @require ONLY FOR PARTNER
      * @param userID $ unique user identifier
      *
      * @return array $array with httpCode and data with all user stands
      */
      public function getUserStands($userID)
     {
        $url =  $this::$BASE_URL_SANDBOX . $this::$GET_STANDS . $userID;
        
        $token = "Authorization: Bearer " . $this->getTokenKey();

        return $this->get($url,array($this::$CONTENT_TYPE_JSON, $token));
     }

     /**
      * @require ONLY FOR PARTNER
      * @param userID $ unique user identifier
      * @param standID $ unique stand identifier
      *
      * @return array $array with httpCode and data with stand details
      */
     public function getStandData($userID, $standID)
     {
        $url =  $this::$BASE_URL_SANDBOX . $this::$GET_STANDS . $userID . $this::$DIRECTORY_CHARACTER . $standID;
        
        $token = "Authorization: Bearer " . $this->getTokenKey();

        return $this->get($url,array($this::$CONTENT_TYPE_JSON, $token));
     }

    /***********************Advert*********************************/

    public function postNewAdvertPartner($advertData)
    {
      $url =  $this::$BASE_URL_SANDBOX . $this::$POST_ADVERT;
      
      $token = "Authorization: Bearer " . $this->getTokenKey();
     
      return $this->post($url, $advertData, array($this::$CONTENT_TYPE_JSON, $token));
    }

     /**
      * @require ONLY FOR PARTNER
      * @param advertID $ unique user identifier
      *
      * @return array $array with httpCode and data with selected advert details
      */
    public function getAdvertDataPartner($advertID)
    {
       $url =  $this::$BASE_URL_SANDBOX . $this::$ADVERT . $advertID;
       
       $token = "Authorization: Bearer " . $this->getTokenKey();

       return $this->get($url,array($this::$CONTENT_TYPE_JSON, $token));
    }

    public function putUpdateAdvertPartner($advertID, $advertData)
    {
        $url =  $this::$BASE_URL_SANDBOX . $this::$ADVERT . $advertID;
  
       $token = "Authorization: Bearer " . $this->getTokenKey();

       return $this->put($url, $advertData, array($this::$CONTENT_TYPE_JSON, $token));
    }

    public function putUpdateExternalAdvert($advertID, $advertData)
    {
        $url =  $this::$BASE_URL_SANDBOX . $this::$ADVERT . $advertID . $this::$ADVERT_EXTERNAL;
  
       $token = "Authorization: Bearer " . $this->getTokenKey();

       return $this->put($url, $advertData, array($this::$CONTENT_TYPE_JSON, $token));
    }

    /**
      * @require ONLY FOR PARTNER
      * @param advertID $ unique advert identifier
      *
      * @return array $array with httpCode 204 if successful and empty data
      */
    public function deleteAdvertPartner($advertID)
     {
        $url =  $this::$BASE_URL_SANDBOX . $this::$ADVERT . $advertID;

        $token = "Authorization: Bearer " . $this->getTokenKey();

        return $this->delete($url, array($this::$CONTENT_TYPE_JSON, $token));
     }

    /***********************Account*********************************/

     /**
      * Contact Object Format
      *     {
      *         "person": "John Doe"
      *     }
      */

      
     /**
      * Coordinate Object Format
      *      {
      *         "latitude": 52.39255,
      *         "longitude": 16.86718
      *      }
      */

      /**
      * Params Object Format
      *       {
      *          "make": "ford",
      *          "model": "focus",
      *          "version": "mk1-1998-2004",
      *          "year": 2003,
      *          "mileage": 199000,
      *          "fuel_type": "petrol",
      *          "body_type": "sedan",
      *          "first_registration_month" : 1,
      *          "first_registration_year" : 2010,
      *          "engine_capacity" : 2522,
      *          "power" : 100,
      *          "section" : 'mini',
      *          "color": "white",
      *          "price":
      *          {
      *              "0": "arranged",
      *              "1": 20000,
      *              "currency": "PLN",
      *              "gross_net": "gross"
      *          },
      *          "video": "https://www.youtube.com/watch?v=code"
      *       }
      */


     /**
      * @param advertData $ Advert object with the following format
      *      title ==> String ==> Title
      *      description ==> String ==> Description
      *      category_id ==> Number ==> Category id , see: Category
      *      region_id ==> Number ==> Region id , see: Regions
      *      city_id ==> Number ==>	City id , see: Cities
      *      district_id ==> Number ==> District id , see: Districts
      *      coordinates ==> Object ==> Location for map coordinates
      *      contact ==> Object ==> Contact object
      *      params	==> Object ==> Detailed advert parameters, see Category to find what parameters are available for specific category
      *      image_collection_id ==> String ==> Id of images collection, see: Images
      *      advertiser_type ==> String ==> Allowed values: private, business
      *      brand_program_id ==> Number ==> Brand program id
      *
      *   
      *
      * @return array $array with httpCode and data with advert object created
      */
    public function postNewAdvert($advertData)
     {
        $url =  $this::$BASE_URL_SANDBOX . $this::$NEW_ADVERT;
      
        $token = "Authorization: Bearer " . $this->getTokenKey();
       
        return $this->post($url, $advertData, array($this::$CONTENT_TYPE_JSON, $token));
     }

     /**
      * @return array $array with httpCode and data with all user adverts
      */
     public function getAllAdverts()
    {
       $url =  $this::$BASE_URL_SANDBOX . $this::$NEW_ADVERT;
       
       $token = "Authorization: Bearer " . $this->getTokenKey();

       return $this->get($url,array($this::$CONTENT_TYPE_JSON, $token));
    }

     /**
      * @param advertID $ unique advert identifier
      * 
      * @return array $array with httpCode and data with all user adverts
      */
    public function getAdvertData($advertID)
    {
       $url =  $this::$BASE_URL_SANDBOX . $this::$NEW_ADVERT_LINK . $advertID;
       
       $token = "Authorization: Bearer " . $this->getTokenKey();

       return $this->get($url,array($this::$CONTENT_TYPE_JSON, $token));
    }

     /**
      * Activate an advert
      * @param advertID $ unique advert identifier
      * 
      * @return array $array with httpCode 204 if successful and empty data
      */
    public function postActivateAdvert($advertID)
     {
        $url =  $this::$BASE_URL_SANDBOX . $this::$NEW_ADVERT_LINK . $advertID . $this::$ADVERT_ACTIVATE;
      
        $token = "Authorization: Bearer " . $this->getTokenKey();
       
        return $this->post($url, array(), array($this::$CONTENT_TYPE_JSON, $token));
     }

     /**
      * Deactivate an advert
      * @param advertID $ unique advert identifier
      * @param advertID $ array with the following format
      *     {
      *         "reason": {
      *         "id": "1",
      *         "description": "Reason to deactivate the Ad"
      *         }
      *     }
      *    
      * @return array $array with httpCode 204 if successful and empty data
      */
     public function postDeactivateAdvert($advertID, $reason)
     {
        $url =  $this::$BASE_URL_SANDBOX . $this::$NEW_ADVERT_LINK . $advertID . $this::$ADVERT_DEACTIVATE;
      
        $token = "Authorization: Bearer " . $this->getTokenKey();
       
        return $this->post($url, $reason, array($this::$CONTENT_TYPE_JSON, $token));
     }

     
     /**
      *
      * @param advertID $ unique advert identifier
      * 
      * @return array $array with httpCode and data with selected advert stats details
      */
     public function getAdvertStats($advertID)
    {
       $url =  $this::$BASE_URL_SANDBOX . $this::$NEW_ADVERT_LINK . $advertID . $this::$ADVERT_STATS;
       
       $token = "Authorization: Bearer " . $this->getTokenKey();

       return $this->get($url,array($this::$CONTENT_TYPE_JSON, $token));
    }

      /**
      * @param advertID $ unique advert identifier
      * 
      * @return array $array with httpCode and data with selected advert stats
      */
     public function getAdvertStatsDetails($advertID)
    {
       $url =  $this::$BASE_URL_SANDBOX . $this::$NEW_ADVERT_LINK . $advertID . $this::$ADVERT_DETAILS;
       
       $token = "Authorization: Bearer " . $this->getTokenKey();

       return $this->get($url,array($this::$CONTENT_TYPE_JSON, $token));
    }

    
      /**
      * @return array $array with httpCode and data with current stats details
      */
    public function getAdvertCurrentStatsDetails()
    {
       $url =  $this::$BASE_URL_SANDBOX . $this::ADVERT_CURRENT_STATS;
       
       $token = "Authorization: Bearer " . $this->getTokenKey();

       return $this->get($url,array($this::$CONTENT_TYPE_JSON, $token));
    }

    /**
      * @return array $array with httpCode and data with current stats
      */
    public function getCurrentStats()
    {
       $url =  $this::$BASE_URL_SANDBOX . $this::$ADVERT_ACCOUNT_STATS;
       
       $token = "Authorization: Bearer " . $this->getTokenKey();

       return $this->get($url,array($this::$CONTENT_TYPE_JSON, $token));
    }

    /**
      * @return array $array with httpCode and data with current stats details
      */
    public function getCurrentStatsDetails()
    {
       $url =  $this::$BASE_URL_SANDBOX . $this::$ADVERT_CURRENT_STATS;
       
       $token = "Authorization: Bearer " . $this->getTokenKey();

       return $this->get($url,array($this::$CONTENT_TYPE_JSON, $token));
    }

      /**
      *
      * @param userData $ array with one of the following formats
      *     {
      *         "user_id": 3243
      *     }
      *             or
      *     {
      *         "email": "example@example.com"
      *     }
      * 
      * @return array $array with httpCode and data with user status
      */
    public function postCurrentUserStatus($userData)
    {
       $url =  $this::$BASE_URL_SANDBOX . $this::$POST_ADVERT_STATUS;
     
       $token = "Authorization: Bearer " . $this->getTokenKey();
      
       return $this->post($url, $userData, array($this::$CONTENT_TYPE_JSON, $token));
    }

    /**
      * @param advertID $ unique advert identifier
      * @param advertData $ object advert check postNewAdvert documentation 
      * 
      * @return array $array with httpCode and data with selected advert stats
      */
    public function putUpdateAdvert($advertID, $advertData)
    {
        $url =  $this::$BASE_URL_SANDBOX . $this::$NEW_ADVERT_LINK . $advertID;
  
       $token = "Authorization: Bearer " . $this->getTokenKey();

       return $this->put($url, $advertData, array($this::$CONTENT_TYPE_JSON, $token));
    }

        /***********************Promotions*********************************/

    /**
     * @param  advertID $ unique advert identifier
     * 
     * @return array $array with httpCode and data all promotions 
     */
    public function getAdvertPromotions($advertID)
    {
        $url =  $this::$BASE_URL_SANDBOX . $this::$ADVERT . $advertID . $this::$PROMOTIONS;
       
        $token = "Authorization: Bearer " . $this->getTokenKey();
 
        return $this->get($url,array($this::$CONTENT_TYPE_JSON, $token));
    }

    /**
     * @param  advertID $ unique advert identifier~
     * @param promotionData $ object advert check postNewAdvert documentation
     * 
     * @return array $array with httpCode and data empty 
     */
    public function postAdvertPromotions($advertID,$promotionData)
    {
        $url =  $this::$BASE_URL_SANDBOX . $this::$ADVERT . $advertID . $this::$PROMOTIONS;
       
        $token = "Authorization: Bearer " . $this->getTokenKey();
 
        return $this->post($url,$promotionData,array($this::$CONTENT_TYPE_JSON, $token));
    }
    
}

$test = new RestAPI(false);

$test->setTokenData($test->userAuthentication("mario@vivasuperstars.com","123456"));


/*$data = array (
  'title' => 'Advert title',
  'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
  'category_id' => 29,
  'region_id' => 1,
  'city_id' => 1,
  'district_id' => 1,
  'coordinates' => 
  array (
    'latitude' => 52.392549999999999954525264911353588104248046875,
    'longitude' => 16.867180000000001172111296909861266613006591796875,
  ),
  'contact' => 
  array (
    'person' => 'John Doe',
  ),
  'params' => 
  array (
    'make' => 'ford',
    'model' => 'focus',
    'version' => 'mk1-1998-2004',
    'year' => 2007,
    'mileage' => 199000,
    'fuel_type' => 'diesel',
    'body_type' => 'sedan',
    'color' => 'white',
    'first_registration_month' => 1,
    'first_registration_year' => 2009,
    'engine_capacity' => 2522,
    'power' => 100,
    'section' => 'mini',
    'price' => 
    array (
      0 => 'arranged',
      1 => 20000,
      'currency' => 'EUR',
      'gross_net' => 'gross',
    ),
    'video' => 'https://www.youtube.com/watch?v=code',
  ),
  'image_collection_id' => 821165359,
);*/

//$data = http_build_query($data);

//echo $test->postNewAdvert($data)['data'];
//echo $test->getAllAdverts()['data'];
//echo $test->getCurrentStats()['data'];
//echo $test->postActivateAdvert(151381)['data'];
//echo $test->postActivateAdvert(151385)['data'];
//echo $test->putUpdateAdvert(151385,$data)['data'];

//$data = array("user_id"=>193);
//echo $test->postCurrentUserStatus($data)['data'];

/*$data = array (
    'reason' => 
    array (
      'id' => '1',
      'description' => 'Reason to deactivate the Ad',
    ),
);
//$data = http_build_query($data);

echo $test->postDeactivateAdvert(151385,$data)['data'];*/

/*$data = array(
    'name'     => 'Name of the stand',
    'status'   => 'active',
    'address'  => 'Stand address',
    'postcode' => '39384-213',
    'email'    => 'standemail@gmail.com',
    'phone1'   => '00441892892',
    'phone2'   => '00441892893',
    'phone3'   => '00441892894',
    'userId'   => '1234',
    'cityId'   => '123123',
    'mapLat'   => '40.200537',
    'mapLon'   => '-8.242188',
);*/

//$data = http_build_query($data)['data'];
//echo $test->postNewAdvert($data)['data'];

//echo $test->postNewStand($data)['data'];
//echo $test->getStandData(93,0)['data'];

//$data = array('user_id'=>"93");
//echo $test->postCurrentUserStatus($data)['data'];
//echo $test->getAllAdverts()['data'];

//$data = array('1'=>'http://media.iolnegocios.pt/media1201/3e0239ad20064c0ef64b3a8a8feae0a6/');
//echo $test->getImageCollection(284429)['data'];
//echo $test->getImageFromCollection(284429,1)['data'];
//$data = array("source"=> "http://media.iolnegocios.pt/media1201/fa72d885c6400812afdf33abc5ecc46e/");
//echo $test->putUpdateImage(284423,1,$data)['data'];
//echo $test->deleteImageFromCollection(284423,1)['data'];
//echo $test->getImageCollection(284423)['data'];
//$data = array('source'=> 'http://media.iolnegocios.pt/media1201/3e0239ad20064c0ef64b3a8a8feae0a6/');
//echo $test->putNewImage(284423,$data)['data'];
//echo $test->getImageCollection(1)['data'];

/*$data = array (
    1 => 'http://media.iolnegocios.pt/media1201/3e0239ad20064c0ef64b3a8a8feae0a6/',
    2 => 'http://media.iolnegocios.pt/media1201/3e0239ad20064c0ef64b3a8a8feae0a6/'
);

echo $test->postImageCollection($data)['data'];*/

//echo $test->getAllRegions()['data'];
//echo $test->getRegionData(12)['data'];
//cho $test->getAllCities()['data'];
//echo $test->getCityData(3153947)['data'];
//echo $test->getAllCountries()['data'];
//echo $test->getCountryData(29)['data'];


//echo $test->getAllDistricts()['data'];
//echo $test->getAllEngineCodeFromModel(29,"mercedes-benz","c-klasa",0,0)['data'];
//echo $test->getAllModelsFromBrand(29,"jaguar",0,0)['data'];
//echo $test->getAllModelVersions(29,"mercedes-benz","gl-63-amg",0,0)['data'];
//echo $test->postAllCities()['data'];
//echo $test->getDistrictData(1054726);//1049603)['data'];
//echo $test->getAllCityDistricts(1049603)['data'];
//$test->partnerAuthentication("mario@vivasuperstars.com","123456")['data'];



//echo $test->getAllCategories(0)['data'];
//echo $test->getCategoryData(29)['data'];


//echo $test->getAllModelsFromBrand(29,"ford")['data'];
?>